package com.martin.demo.elasticsearch.api;

import com.martin.demo.elasticsearch.domain.Book;
import com.martin.demo.elasticsearch.persistence.elasticsearch.BookEsRepository;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/api")
public class MyController {

    private final BookEsRepository repository;

    public MyController(BookEsRepository repository) {
        this.repository = repository;
    }

    @PutMapping("books")
    public Book createBook()  {
        Book book = new Book("Harry Potter", "Charles Dickens", LocalDate.now().minusMonths(2).toString());
        return repository.save(book);
    }
}
