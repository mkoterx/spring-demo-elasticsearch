package com.martin.demo.elasticsearch.persistence;

import com.martin.demo.elasticsearch.domain.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.NonNull;

import java.util.List;

@NoRepositoryBean
public interface BaseBookRepository extends BaseRepository<Book, String> {

    Page<Book> findByAuthor(@NonNull String author, Pageable pageable);

    List<Book> findByTitle(@NonNull String title);
}
