package com.martin.demo.elasticsearch.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.martin.demo.elasticsearch.persistence.elasticsearch")
public class ElasticsearchConfig {

//    If you want to manually configure the creation of the Elasticsearch Client (Problems with Netty3 Plugin)

//    @Bean
//    public Client transportClient() throws UnknownHostException {
//        Settings settings = Settings.builder()
//                .put("cluster.name", clusterName).build();
//
//        return new PreBuiltTransportClient(settings)
//                    .addTransportAddress(new TransportAddress(InetAddress.getByName(host), 9300));
//    }
//
//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() throws Exception {
//        return new ElasticsearchTemplate(transportClient());
//    }
}
