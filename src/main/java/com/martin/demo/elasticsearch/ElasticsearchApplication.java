package com.martin.demo.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ElasticsearchApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(ElasticsearchApplication.class, args);
	}
}
